package tn.permiya.security;

import java.util.List;

public class JwtResponse {
	
	private String jwt;
	
	private Long id;
	
	private String email ;
	
	private List<String>roles ;

	public JwtResponse(String jwt, Long id, String email, List<String> roles) {
		super();
		this.jwt = jwt;
		this.id = id;
		this.email = email;
		this.roles = roles;
	}

	public String getJwt() {
		return jwt;
	}

	public Long getId() {
		return id;
	}

	public String getEmail() {
		return email;
	}

	public List<String> getRoles() {
		return roles;
	}

	public void setJwt(String jwt) {
		this.jwt = jwt;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}
	
	
	

}
