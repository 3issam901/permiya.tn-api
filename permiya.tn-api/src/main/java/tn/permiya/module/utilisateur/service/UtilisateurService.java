package tn.permiya.module.utilisateur.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import tn.permiya.module.utilisateur.entite.Utilisateur;
import tn.permiya.module.utilisateur.repository.UtilisateurRepository;

@Component
public class UtilisateurService {

	@Autowired
	private UtilisateurRepository utilisateurDao;

	@Autowired
	private PasswordEncoder encoder;

	public Optional<Utilisateur> getUtilisateurById(Long idUtilisateur) {
		return utilisateurDao.findById(idUtilisateur);
	}

	public Utilisateur ajouterOuModifierUtilisateur(Utilisateur utilisateur) {
		if (utilisateur.getIdUtilisateur() == 0) {
			utilisateur.setMotPasse(encoder.encode(utilisateur.getMotPasse()));
		}
		return utilisateurDao.save(utilisateur);
	}

	public void supprimerUtilisateur(Utilisateur utilisateur) {
		utilisateurDao.delete(utilisateur);
	}

	public Iterable<Utilisateur> listUtilisateurs() {
		return utilisateurDao.findAll();
	}

}
