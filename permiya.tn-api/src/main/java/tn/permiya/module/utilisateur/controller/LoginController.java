package tn.permiya.module.utilisateur.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tn.permiya.module.utilisateur.entite.Utilisateur;
import tn.permiya.module.utilisateur.entite.UtilisateurDetailsImpl;
import tn.permiya.security.JwtResponse;
import tn.permiya.security.JwtUtils;

@RestController
@RequestMapping(path = "/api/auth")
public class LoginController {

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtUtils jwtUtils;

	@PostMapping(path = "/login", consumes = "application/json", produces = "application/json")
	public ResponseEntity<JwtResponse> login(@RequestBody Utilisateur utilisateur) {
		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(utilisateur.getEmail(), utilisateur.getMotPasse()));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtUtils.generateJwtToken(authentication);

		UtilisateurDetailsImpl utilisateurDetails = (UtilisateurDetailsImpl) authentication.getPrincipal();
		List<String> roles = utilisateurDetails.getAuthorities().stream().map(item -> item.getAuthority())
				.collect(Collectors.toList());

		return ResponseEntity.ok(new JwtResponse(jwt, utilisateurDetails.getId(), utilisateur.getEmail(), roles));

	}

}
