package tn.permiya.module.utilisateur.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tn.permiya.module.utilisateur.entite.Utilisateur;
import tn.permiya.module.utilisateur.service.UtilisateurService;

/**
 * 
 * @author Permiya.tn [Hichem Nasri] Utilisateur Controller
 *
 */

@RestController
@RequestMapping(path = "/api/utilisateurs")
public class UtilisateurController {

	@Autowired
	private UtilisateurService utilisateurService;
	

	

	@GetMapping(path = "/liste", produces = "application/json")
	public ResponseEntity<Iterable<Utilisateur>> listerUtilisateurs() {
		Iterable<Utilisateur> utilisateurs = utilisateurService.listUtilisateurs();
		return ResponseEntity.ok(utilisateurs);
	}

	@PostMapping(path = "/ajouter", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Utilisateur> ajouterUtilisateur(@RequestBody Utilisateur utilisateur) {
		Utilisateur utilisateurAjoute = utilisateurService.ajouterOuModifierUtilisateur(utilisateur);
		return ResponseEntity.ok(utilisateurAjoute);

	}

	@PutMapping(path = "/modifier", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Utilisateur> modifierUtilisateur(@RequestBody Utilisateur utilisateur) {
		Utilisateur utilisateurModifie = utilisateurService.ajouterOuModifierUtilisateur(utilisateur);
		return ResponseEntity.ok(utilisateurModifie);
	}

	@DeleteMapping(path = "/supprimer/{id}", produces = "application/json")
	public ResponseEntity<Iterable<Utilisateur>> supprimerUtilisateur(@PathVariable Long id) {
		Optional<Utilisateur> utilisateurSupprime = utilisateurService.getUtilisateurById(id);
		utilisateurService.supprimerUtilisateur(utilisateurSupprime.get());
		Iterable<Utilisateur> utilisateurs = utilisateurService.listUtilisateurs();
		return ResponseEntity.ok(utilisateurs);

	}

}
