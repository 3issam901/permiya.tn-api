package tn.permiya.module.utilisateur.entite;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author Permiya.tn Entite Utilisateur image Java du TA_UTILISATEUR
 *
 */
@Entity
@Table(name = "TA_UTILISATEUR", uniqueConstraints = { @UniqueConstraint(columnNames = "email") })
public class Utilisateur {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_UTILISATEUR")
	protected long idUtilisateur;

	@Column(name = "NOM")
	protected String nom;

	@Column(name = "PRENOM")
	protected String prenom;

	@Column(name = "EMAIL", nullable = false)
	protected String email;

	@Column(name = "MOT_PASSE", nullable = false)
	protected String motPasse;

	@Column(name = "NUMERO_TELEPHONE")
	protected String numeroTelephone;

	@Column(name = "PHOTO_PROFILE")
	protected byte[] photoProfile;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "UTILISATEURS_ROLE", joinColumns = @JoinColumn(name = "ID_UTILISATEUR"), inverseJoinColumns = @JoinColumn(name = "ID_ROLE"))
	protected Set<Role> roles = new HashSet<Role>();

	public Utilisateur() {

	}

	public Utilisateur(String nom, String prenom, String email, String motPasse, String numeroTelephone,
			byte[] photoProfile, Set<Role> roles) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.motPasse = motPasse;
		this.numeroTelephone = numeroTelephone;
		this.photoProfile = photoProfile;
		this.roles = roles;
	}

	public long getIdUtilisateur() {
		return idUtilisateur;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMotPasse() {
		return motPasse;
	}

	public void setMotPasse(String motPasse) {
		this.motPasse = motPasse;
	}

	public String getNumeroTelephone() {
		return numeroTelephone;
	}

	public void setNumeroTelephone(String numeroTelephone) {
		this.numeroTelephone = numeroTelephone;
	}

	public byte[] getPhotoProfile() {
		return photoProfile;
	}

	public void setPhotoProfile(byte[] photoProfile) {
		this.photoProfile = photoProfile;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

}
