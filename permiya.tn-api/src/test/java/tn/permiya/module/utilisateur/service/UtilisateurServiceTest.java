package tn.permiya.module.utilisateur.service;

import static org.junit.Assert.assertEquals;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import tn.permiya.module.utilisateur.entite.NomRole;
import tn.permiya.module.utilisateur.entite.Role;
import tn.permiya.module.utilisateur.entite.Utilisateur;
import tn.permiya.module.utilisateur.repository.RoleRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations = "classpath:applicationtest.properties")

/**
 * 
 * @author Permiya.tn Classe de test UtilisateurService
 */
public class UtilisateurServiceTest {

	@Autowired
	private UtilisateurService utilisateurService;
	
	@Autowired
	private RoleRepository roleRepository;
	
	private Utilisateur utilisateur;
	

	@Before
	public void setUp() {
		NomRole nomRole=NomRole.ROLE_UTILISATEUR;
		Role roleUtilisateur=new Role(nomRole);
		roleUtilisateur=roleRepository.save(roleUtilisateur);
		Set<Role>roles =new HashSet<Role>();
		roles.add(roleUtilisateur);
		utilisateur = new Utilisateur("testnom", "testprenom", "test@gmail.com", "test123", "0632233213", null,roles);
		utilisateurService.ajouterOuModifierUtilisateur(utilisateur);
	}

	/**
	 * Tester ajout d'utilisateur dans la db
	 */
	@Test
	public void testAjouterUtilisateur() {
		Utilisateur utilisateurAjoute = utilisateurService.ajouterOuModifierUtilisateur(utilisateur);
		Optional<Utilisateur> expectedUtilisateur = utilisateurService
				.getUtilisateurById(utilisateur.getIdUtilisateur());

		assertEquals(expectedUtilisateur.get().getIdUtilisateur(), utilisateurAjoute.getIdUtilisateur());
		assertEquals(expectedUtilisateur.get().getNom(), utilisateurAjoute.getNom());
		assertEquals(expectedUtilisateur.get().getPrenom(), utilisateurAjoute.getPrenom());
		assertEquals(expectedUtilisateur.get().getEmail(), utilisateurAjoute.getEmail());
		assertEquals(expectedUtilisateur.get().getMotPasse(), utilisateurAjoute.getMotPasse());
		assertEquals(expectedUtilisateur.get().getNumeroTelephone(), utilisateurAjoute.getNumeroTelephone());
	}

	/**
	 * Tester modification d'utilisateur dans la db
	 */
	@Test
	public void testModifierUtilisateur() {
		this.utilisateur.setNom("testnommodif");
		Utilisateur utilisateurModifie = utilisateurService.ajouterOuModifierUtilisateur(utilisateur);
		Optional<Utilisateur> expectedUtilisateur = utilisateurService
				.getUtilisateurById(utilisateur.getIdUtilisateur());
		assertEquals(expectedUtilisateur.get().getNom(), utilisateurModifie.getNom());

	}

	/**
	 * Tester suppression d'utilisateur dans la db
	 */
	@Test
	public void testSupprimerUtilisateur() {
		utilisateurService.supprimerUtilisateur(utilisateur);
		Optional<Utilisateur> expectedUtilisateur = utilisateurService
				.getUtilisateurById(utilisateur.getIdUtilisateur());
		assertEquals(expectedUtilisateur, Optional.empty());
	}
  
}
